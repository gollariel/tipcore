<?php
/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 3/1/17
 * Time: 14:02
 */
class ManagerHelper
{
	protected $managerList = [];
	protected static $instance;

	public function __construct()
	{
		$config = json_decode("{ \"sng\": { \"15\": { \"9\": { \"2\": 1 } } }, \"texas\": { \"15\": { \"5\": { \"2\": 1, \"4\": 1, \"8\": 1, \"10\": 1, \"20\": 1, \"40\": 1, \"80\": 1, \"100\": 1, \"200\": 1, \"400\": 1, \"800\": 1, \"1000\": 1, \"2000\": 1, \"4000\": 1, \"8000\": 1, \"10000\": 1, \"20000\": 1, \"40000\": 1, \"80000\": 1 }, \"9\": { \"2\": 1, \"4\": 1, \"8\": 1, \"10\": 1, \"20\": 1, \"40\": 1, \"80\": 1, \"100\": 1, \"200\": 1, \"400\": 1, \"800\": 1, \"1000\": 1, \"2000\": 1, \"4000\": 1, \"8000\": 1, \"10000\": 1, \"20000\": 1, \"40000\": 1, \"80000\": 1 } } } }", true);

		$this->managerList = $config;
	}

	public static function __callStatic($name, $arguments)
	{
		if (!isset(static::$instance)) {
			static::$instance = new static();
		}
		return static::$instance->{$name}(...$arguments);
	}

	private function getManagerList()
	{
		return $this->managerList;
	}

	private function validateManagerId($managerId)
	{
		$tmp = explode(':', $managerId);

		if (count($tmp) === 4 && isset($this->managerList[$tmp[3]][$tmp[2]][$tmp[1]][$tmp[0]])) {
			list($blind, $seats, $speed, $type) = $tmp;
			return ['blind' => $blind, 'seats' => $seats, 'speed' => $speed, 'type' => $type];
		} else {
			return null;
		}
	}
}

$managerList = ManagerHelper::getManagerList();
var_dump(ManagerHelper::validateManagerId("4:9:15:sng"));
