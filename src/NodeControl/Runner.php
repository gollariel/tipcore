<?php
namespace TIP\Core\NodeControl;

use TkachInc\CLI\Process\ProcessHelper;

/**
 * Class Runner
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Runner
{
//	protected static $pattern = "%s %s/main.php -r %s %s -l %s %s -vvvv";
	protected static $pattern = "%s %s/main.php -r %s %s %s %s";

	public static function start($className, $mainPath, $enable = true, $params = '', $arguments = [])
	{
		$binary = ProcessHelper::getPHPBinary();

		if ($enable) {
			$action = '-e';
		} else {
			$action = '-s';
		}

		return sprintf(static::$pattern, $binary, $mainPath, str_replace('\\', '/', str_replace('CLIController\\', '', $className)) . '/run', $action, $params, '-a ' . http_build_query($arguments, null, '+'));
	}
}