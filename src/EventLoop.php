<?php
namespace TIP\Core;

use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;

/**
 * Class EventLoop
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class EventLoop
{
	/**
	 * @var LoopInterface
	 */
	protected static $loop;

	/**
	 * @return LoopInterface
	 */

	public static function get():LoopInterface
	{
		if (!isset(static::$loop)) {
			static::$loop = Factory::create();
		}

		return static::$loop;
	}
}