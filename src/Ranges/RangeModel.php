<?php
namespace TIP\Core\Ranges;
use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class RangeModel extends ObjectModel
{
	/** @var string $collection Название модели */
	protected static $_collection = 'pk_range_config';
	/** @var string $pk первичный ключ */
	protected static $_pk = '_id';
	/** @var array $sort параметры сортировки */
	protected static $_sort = ['_id' => 1];
	/** @var array $indexes индексы коллекции */
	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],
		[
			'keys' => ['card1' => 1],
		],
		[
			'keys' => ['card2' => 1],
		],
	];

	/** @var array $fieldsDefault список значений коллекции */
	protected static $_fieldsDefault = [
		'_id'   => 0,
		'card1' => '',
		'card2' => '',
	];

	/** @var array $fieldsValidate типы значений для валидации */
	protected static $_fieldsValidate = [
		'_id'   => self::TYPE_UNSIGNED_INT,
		'card1' => self::TYPE_STRING,
		'card2' => self::TYPE_STRING,
	];
}