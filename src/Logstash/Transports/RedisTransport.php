<?php
namespace TIP\Core\Logstash\Transports;
use TkachInc\Core\Database\Connections\RedisDatabase;


/**
 * @author maxim
 */
class RedisTransport implements TransportInterface
{
	protected $key;
	protected $method;

	/**
	 * RedisTransport constructor.
	 * @param $key
	 * @param $dataType
	 */
	public function __construct($key, $dataType)
	{
		$this->key = $key;
		switch ($dataType) {
			case 'pattern_channel':
			case 'channel':
				$this->method = 'publish';
				break;
			case 'list':
			default:
				$this->method = 'rpush';
				break;
		}
	}

	/**
	 * @param $message
	 * @return mixed
	 */
	public function send($message)
	{
		$redis = RedisDatabase::getInstance();
		$method = $this->method;

		return $redis->{$method}($this->key, $message);
	}
}