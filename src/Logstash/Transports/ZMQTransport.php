<?php
namespace TIP\Core\Logstash\Transports;

use TIP\Core\Transport\ZMQInstance;

/**
 * @author maxim
 */
class ZMQTransport implements TransportInterface
{
	protected $host;
	protected $type;

	/**
	 * ZMQTransport constructor.
	 * @param $host
	 * @param $topology
	 */
	public function __construct($host, $topology)
	{
		$this->host = $host;
		switch ($topology) {
			case 'pair':
				$this->type = \ZMQ::SOCKET_PAIR;
				break;
			case 'pushpull':
				$this->type = \ZMQ::SOCKET_PUSH;
				break;
			case 'pubsub':
			default:
				$this->type = \ZMQ::SOCKET_PUB;
				break;
		}
	}

	/**
	 * @param $message
	 * @return \ZMQSocket
	 */
	public function send($message)
	{
		$zmq = ZMQInstance::getConnect($this->host, $this->type);
		$result = $zmq->send($message);
		ZMQInstance::disconnect();

		return $result;
	}
}