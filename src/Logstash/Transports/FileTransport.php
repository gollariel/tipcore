<?php
namespace TIP\Core\Logstash\Transports;

/**
 * @author maxim
 */
class FileTransport implements TransportInterface
{
	protected $key;

	/**
	 * FileTransport constructor.
	 * @param $key
	 */
	public function __construct($key)
	{
		$this->key = $key;
	}

	/**
	 * @param $message
	 * @return int
	 */
	public function send($message)
	{
		if (!file_exists('/tmp/tiplog')) {
			mkdir('/tmp/tiplog');
		}

		return file_put_contents('/tmp/tiplog/' . $this->key . '.out', $message . PHP_EOL, FILE_APPEND);
	}
}