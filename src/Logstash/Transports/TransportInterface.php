<?php
namespace TIP\Core\Logstash\Transports;

/**
 * @author maxim
 */
interface TransportInterface
{
	/**
	 * @param $message
	 * @return mixed
	 */
	public function send($message);
}