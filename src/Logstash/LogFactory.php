<?php
namespace TIP\Core\Logstash;

use TIP\Core\Logstash\Transports\FileTransport;
use TIP\Core\Logstash\Transports\RedisTransport;
use TIP\Core\Logstash\Transports\ZMQTransport;
use TkachInc\Core\Config\Config;

/**
 * @author maxim
 */
class LogFactory
{
	/**
	 * @var array
	 */
	public static $transports = [];
	/**
	 * @var Logstash
	 */
	public static $logstash;

	/**
	 * @param $message
	 * @param string $transportType
	 */
	public static function send($message, $transportType = 'redis')
	{
		if (!isset(static::$transports[$transportType])) {
			switch ($transportType) {
				case 'file':
					static::$transports[$transportType] = new FileTransport('logstash-' . date('Y.m.d'));
					break;
				case 'zmq':
					static::$transports[$transportType] = new ZMQTransport(Config::getInstance()->get(['logstash',
						'zmq',
						'host'],
						'127.0.0.1:2120'),
						Config::getInstance()->get(['logstash',
							'zmq',
							'topology'],
							'pubsub'));
					break;
				case 'redis':
				default:
					static::$transports[$transportType] = new RedisTransport('logstash',
						Config::getInstance()->get(['logstash',
							'redis',
							'dataType'],
							'list'));
					break;
			}
		}

		if (!isset(static::$logstash)) {
			static::$logstash = new Logstash(static::$transports[$transportType]);
		}

		if (isset(static::$transports[$transportType])) {
			static::$logstash->setTransport(static::$transports[$transportType]);
		}

		static::$logstash->send($message);
	}
}