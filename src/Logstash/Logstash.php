<?php
namespace TIP\Core\Logstash;

use TIP\Core\Logstash\Transports\TransportInterface;

/**
 * @author maxim
 */
class Logstash
{
	/**
	 * @var TransportInterface
	 */
	protected $transport;

	/**
	 * Logstash constructor.
	 * @param TransportInterface $transport
	 */
	public function __construct(TransportInterface $transport)
	{
		$this->setTransport($transport);
	}

	/**
	 * @param TransportInterface $transport
	 */
	public function setTransport(TransportInterface $transport)
	{
		$this->transport = $transport;
	}

	/**
	 * @param $message
	 */
	public function send($message)
	{
		if (is_array($message) || is_object($message)) {
			$message = json_encode($message);
		}

		if ($message) {
			$this->transport->send($message);
		}
	}
}