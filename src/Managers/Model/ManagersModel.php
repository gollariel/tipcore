<?php
namespace TIP\Core\Managers\Model;

use TIP\Core\RedisAdapter\ObjectModelSortedSets;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ManagersModel extends ObjectModelSortedSets
{
	protected static $_keyName = 'pk_manager';

	/**
	 * @param $blind
	 * @param $seats
	 * @param $time
	 * @return string
	 */
	public static function makeManagerName($blind, $seats, $time)
	{
		return $blind . static::$_separator . $seats . static::$_separator . $time;
	}
} 