<?php
namespace TIP\Core\Managers\Model;
use TkachInc\Core\Database\Redis\ObjectModel;

/**
 * @property int blind
 * @property int stack
 * @property mixed pos
 * @property mixed board
 * @property mixed reserved
 * @property mixed isRun
 * @property mixed sbPos
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class GameRedisModel extends ObjectModel
{
	protected static $_separator = ':';
	protected static $_keyName = 'pk_game';
	protected static $_fieldsDefault = [
		'isRun'       => false, // true after create thread
		'isStarted'       => false,
		'deck'        => [],
		'seek'        => 0,
		'time'        => 0,
		'stacks'      => [],
		'board'       => [],
		'state'       => '',
		'round'       => '',
		'currentUser' => '',
	];
	protected static $_fieldsValidate = [
		'isRun'       => self::TYPE_BOOL,
		'isStarted'       => self::TYPE_BOOL, // true after start game loop
		'deck'        => self::TYPE_JSON,
		'seek'        => self::TYPE_INT,
		'time'        => self::TYPE_INT,
		'stacks'      => self::TYPE_JSON,
		'board'       => self::TYPE_JSON,
		'state'       => self::TYPE_STRING,
		'round'       => self::TYPE_STRING,
		'currentUser' => self::TYPE_STRING,
	];
}