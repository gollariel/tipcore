<?php
namespace TIP\Core;

/**
 * Class Timers
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Timers
{
	protected static $instances = [];
	protected $timers = [];

	/**
	 * @param $userId
	 * @return $this
	 * @throws \Exception
	 */
	public static function getInstance($userId)
	{
		if(!isset(static::$instances[$userId]))
		{
			static::$instances[$userId] = new static();
		}
		return static::$instances[$userId];
	}

	/**
	 * @param          $name
	 * @param          $timeout
	 * @param \Closure $function
	 */
	public function addPeriodicTimer($name, $timeout, \Closure $function)
	{
		if (isset($this->timers[$name])) {
			static::remTimer($name);
		}
		$this->timers[$name] = EventLoop::get()->addPeriodicTimer(
			$timeout,
			$function
		);
	}

	/**
	 * @param          $name
	 * @param          $timeout
	 * @param \Closure $function
	 */
	public function addTimer($name, $timeout, \Closure $function)
	{
		if (isset($this->timers[$name])) {
			static::remTimer($name);
		}
		$this->timers[$name] = EventLoop::get()->addTimer(
			$timeout,
			$function
		);
	}

	/**
	 * @param $name
	 * @return bool
	 */
	public function existTimer($name)
	{
		return isset($this->timers[$name]);
	}


	/**
	 * @param $name
	 */
	public function remTimer($name)
	{
		if (isset($this->timers[$name])) {
			EventLoop::get()->cancelTimer($this->timers[$name]);
			unset($this->timers[$name]);
		}
	}

	public function closeAll()
	{
		foreach ($this->timers as $name => $timer) {
			EventLoop::get()->cancelTimer($timer);
			unset($this->timers[$name]);
		}
	}

	public static function remInstance($userId)
	{
		if(isset(static::$instances[$userId]))
		{
			static::$instances[$userId]->closeAll();
			unset(static::$instances[$userId]);
		}
	}
}