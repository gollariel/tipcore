<?php
namespace TIP\Core;

/**
 * Class ActionManager
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ActionManager
{
	const RESPONSE_SMALL_STACK = 'small_stack';
	const RESPONSE_GAME_START = 'game_start';
	const RESPONSE_BLINDS = 'blinds';
	const RESPONSE_BOARD = 'board';
	const RESPONSE_CARDS = 'cards';
	const RESPONSE_ACT = 'act';
	const RESPONSE_USER_ACTION = 'user_action';
	const RESPONSE_PLAYER_WIN = 'player_win';
	const RESPONSE_GAME_END = 'game_end';
	const RESPONSE_GAME_CLOSE = 'game_close';
	const RESPONSE_BALANCE = 'balance';
	const RESPONSE_BACK_BET = 'back_bet';
	const RESPONSE_STACK = 'stack';
	const RESPONSE_EXIT_SITOUT = 'exit_sitout';
	const RESPONSE_SITOUT = 'sitout';
	const RESPONSE_NEW_USER = 'new_user';
	const RESPONSE_REM_USER = 'rem_user';
	const RESPONSE_SUCCESS_LEAVE = 'success_leave';
	const RESPONSE_GAME_INFO = 'game_info';
}