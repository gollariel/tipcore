<?php
namespace TIP\Core\Users\Model;

/**
 * Created by PhpStorm.
 * User: Maksym Tkach
 * Date: 4/25/17
 * Time: 22:29
 * @property int  lockBalance
 */
class UserCacheModel extends \TkachInc\BaseUser\Model\UserCacheModel
{
	const GAME_TYPE_SNG = 'sng';
	const GAME_TYPE_TEXAS = 'texas';
	const GAME_TYPE_PINEAPPLE = 'pineapple';

	protected $availableTypes = [
		self::GAME_TYPE_SNG => 1,
		self::GAME_TYPE_TEXAS => 1,
		self::GAME_TYPE_PINEAPPLE => 1,
	];

	protected static $_separator = ':';

	protected static $_keyName = 'cache_user';

	protected static $_fieldsDefault = [
		'sessionId'      => '',
		'balance'        => 0,
		'lockBalance'        => 0,
		'managerId'      => '',
		'gameId'         => '',
		'stack'          => 0,
		'pos'            => '',
		'autobuyin'      => false,
		'payload'        => [],
		'timerToWatcher' => 0,
		'server'         => '',
		'state'          => '',
		'active'      => 0,
	];

	protected static $_fieldsValidate = [
		'sessionId'      => self::TYPE_STRING,
		'balance'        => self::TYPE_UNSIGNED_INT,
		'lockBalance'        => self::TYPE_UNSIGNED_INT,
		'managerId'      => self::TYPE_STRING,
		'gameId'         => self::TYPE_STRING,
		'stack'          => self::TYPE_UNSIGNED_INT,
		'pos'            => self::TYPE_STRING,
		'autobuyin'      => self::TYPE_BOOL,
		'payload'        => self::TYPE_JSON,
		'timerToWatcher' => self::TYPE_TIMESTAMP,
		'server'         => self::TYPE_STRING,
		'state'          => self::TYPE_STRING,
		'active'      => self::TYPE_TIMESTAMP,
	];

	protected static $_fieldsPrivate = ['sessionId' => 1];

	public function isSNG($type) {
		return $type === self::GAME_TYPE_SNG;
	}
	public function isTexas($type) {
		return $type === self::GAME_TYPE_TEXAS;
	}
	public function isPineapple($type) {
		return $type === self::GAME_TYPE_PINEAPPLE;
	}
	public function setGameType($type) {
		return isset($this->availableTypes[$type])?true:false;
	}
}