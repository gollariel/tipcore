<?php
namespace TIP\Core\Transport;

use React\ZMQ\Context;
use React\ZMQ\SocketWrapper;
use TIP\Core\EventLoop;

/**
 * Class ZMQInstance
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ReactZMQInstance
{
	protected static $instance;
	protected static $context = [];

	/**
	 * @return \ZMQContext
	 */
	public static function getInstance()
	{
		if (!isset(static::$instance)) {
			static::$instance = new Context(EventLoop::get());
		}

		return static::$instance;
	}

	/**
	 * @param int $type
	 * @param     $host
	 * @return SocketWrapper
	 */
	public static function getContext(int $type = \ZMQ::SOCKET_SUB, string $host):SocketWrapper
	{
		if (!isset(static::$context[$type])) {
			static::$context[$type] = static::getInstance()->getSocket($type);
		}
		/* Get list of connected endpoints */
		$endpoints = static::$context[$type]->getEndpoints();
		/* Check if the socket is connected */
		if (!in_array($host, $endpoints['connect'])) {
			static::$context[$type]->connect($host);
		}

		return static::$context[$type];
	}

	/**
	 * @param int $type
	 * @param     $host
	 */
	public static function disconnect($type = \ZMQ::SOCKET_SUB, $host)
	{
		if (isset(static::$context[$type])) {
			static::$context[$type]->disconnect($host);
		}
	}

}