<?php
namespace TIP\Core\Transport;

/**
 * Class ZMQTransport
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ZMQTransport
{
	/**
	 * @param        $key
	 * @param string $host
	 * @param null $timeout
	 * @param null $message
	 * @return null|string
	 */
	public static function listenMessageByTimeout($key, $host, $timeout = null, $message = null)
	{
		$timeout = $timeout * 1000;
		try {
			//var_dump('CONNECT');
			$context = new \ZMQContext();
			$subscriber = new \ZMQSocket($context, \ZMQ::SOCKET_SUB);
			$subscriber->connect($host);
			$subscriber->setSockOpt(\ZMQ::SOCKOPT_SUBSCRIBE, $key);
			if (isset($timeout) && is_int($timeout)) {
				$subscriber->setSockOpt(\ZMQ::SOCKOPT_RCVTIMEO, $timeout);
			}
			//var_dump('SUBSCRIBE');

			while (true) {
				//  Read envelope with address
				$message = $subscriber->recv();
				$more = $subscriber->getSockOpt(\ZMQ::SOCKOPT_RCVMORE);
				if ($more) {
					//  Read message contents
					$message = $subscriber->recv();
				}
				break;
			}
			//var_dump('DISCONNECT');
			$subscriber->disconnect($host);
			unset($subscriber);
			unset($context);
		} catch (\Exception $e) {
			//var_dump('ERROR ZMQ '.$e->getMessage());
		}

		return $message;
	}

	/**
	 * @param        $message
	 * @param        $key
	 * @param string $host
	 */
	public static function publishMessage($message, $key, $host)
	{
		$context = new \ZMQContext();
		$publisher = new \ZMQSocket($context, \ZMQ::SOCKET_PUB);
		$publisher->connect($host);
		$publisher->send($key, \ZMQ::MODE_SNDMORE);
		$publisher->send(json_encode($message));
		$publisher->disconnect($host);
		unset($publisher);
		unset($context);
	}
}