<?php
namespace TIP\Core\Transport;

/**
 * Class ZMQInstance
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ZMQInstance
{
	protected static $instance;
	protected static $connect;
	protected static $host;

	/**
	 * @return \ZMQContext
	 */
	public static function getInstance()
	{
		if (!isset(static::$instance)) {
			static::$instance = new \ZMQContext();
		}

		return static::$instance;
	}

	/**
	 * @param     $host
	 * @param int $type
	 * @return \ZMQSocket
	 */
	public static function getConnect($host, $type = \ZMQ::SOCKET_SUB)
	{
		if (!isset(static::$connect)) {
			static::$host = $host;
			static::getInstance();
			static::$connect = static::$instance->getSocket($type);
			static::$connect->connect(static::$host);
		}
		/* Get list of connected endpoints */
		$endpoints = static::$connect->getEndpoints();
		/* Check if the socket is connected */
		if (!in_array(static::$host, $endpoints['connect'])) {
			static::$connect->connect(static::$host);
		}

		return static::$connect;
	}

	public static function disconnect()
	{
		if (isset(static::$connect)) {
			static::$connect->disconnect(static::$host);
		}
	}

}