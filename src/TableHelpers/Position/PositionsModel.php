<?php
namespace TIP\Core\TableHelpers\Position;
use TkachInc\Core\Database\Redis\ObjectModel;

/**
 * Class PositionsModel
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PositionsModel extends ObjectModel
{
	protected static $_separator = ':';
	protected static $_keyName = 'pk_positions';

	/**
	 * @param $id
	 * @param $hashKey
	 * @param $value
	 * @return int
	 */
	public static function hSet($id, $hashKey, $value)
	{
		$key = static::makePk($id);

		return static::getRedis()->hSet($key, $hashKey, $value);
	}

	/**
	 * @param $id
	 * @param $hashKey
	 * @return string
	 */
	public static function hGet($id, $hashKey)
	{
		$key = static::makePk($id);
		$value = static::getRedis()->hGet($key, $hashKey);

		return $value;
	}

	/**
	 * @param $id
	 * @return array
	 */
	public static function hGetAll($id)
	{
		$key = static::makePk($id);
		$fields = (array)static::getRedis()->hGetAll($key);

		return $fields;
	}

	/**
	 * @param $id
	 * @param $hashKeys
	 * @return bool
	 */
	public static function hMset($id, Array $hashKeys)
	{
		$key = static::makePk($id);

		return static::getRedis()->hMset($key, $hashKeys);
	}

	/**
	 * @param $id
	 * @param $hashKeys
	 * @return array
	 */
	public static function hMGet($id, Array $hashKeys)
	{
		$key = static::makePk($id);
		$fields = (array)static::getRedis()->hMGet($key, $hashKeys);

		return $fields;
	}
}