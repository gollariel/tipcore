<?php
namespace TIP\Core\TableHelpers\Position;

use TIP\Core\Managers\Model\ManagersModel;
use TIP\Core\TableHelpers\Manager;
use TIP\Core\TableHelpers\Status\Model\SitoutedPlayers;
use TIP\Core\Users\Model\UserCacheModel;

/**
 * Class PositionManager
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PositionManager
{
	public static $rounds = ['preflop', 'flop', 'turn', 'river'];
	public static $possibleActions = ['check' => 1, 'fold' => 1, 'call' => 1, 'bet' => 1, 'raise' => 1, 'allin' => 1];

	/**
	 * @param string $gameId
	 * @param int $sits
	 * @return PositionsIterator
	 */
	public static function geIteratorByGame(string $gameId, int $sits)
	{
		$players = PositionsModel::hGetAll($gameId);

		return new PositionsIterator($players, $sits);
	}

	/**
	 * @param string $gameId
	 * @param string $userId
	 */
	public static function hardClear(string $gameId, string $userId)
	{
		$players = PositionsModel::hGetAll($gameId);
		$key = array_search($userId, $players);
		if ($key !== false) {
			PositionsModel::hSet($gameId, $key, '');
		}
	}

	/**
	 * @param Manager        $manager
	 * @param string         $gameId
	 * @param string         $pos
	 * @param UserCacheModel $userCache
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function sit(Manager $manager, string $gameId, $pos, UserCacheModel $userCache):bool
	{
		$players = PositionsModel::hGetAll($gameId);

		$iterator = new PositionsIterator($players, $manager->getSeats());
		if ($pos === null) {
			$poss = $iterator->getArrayCopy();
			foreach ($poss as $pos => $u) {
				if (!$u) {
					break;
				}
			}
		}

		if (empty($iterator[$pos])) {
			$userCache->pos = $pos;

			$iterator[$pos] = $userCache->_id;
			$success = true;
			PositionsModel::hMset($gameId, $iterator->getArrayCopy());
			ManagersModel::zIncrBy($manager->getManagerId(), 1, $gameId);
		}
		else{
			throw new \Exception('Position is not free: '.$pos);
		}

		return $success;
	}

	/**
	 * @param Manager        $manager
	 * @param string         $gameId
	 * @param                $pos
	 * @param UserCacheModel $userCache
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function sitAfterReserve(Manager $manager, string $gameId, $pos, UserCacheModel $userCache):bool
	{
		$success = false;
		$players = PositionsModel::hGetAll($gameId);

		$iterator = new PositionsIterator($players, $manager->getSeats());
		if ($pos === null) {
			return $success;
		}

		if ($iterator[$pos] === PositionsIterator::RESERVED) {
			$userCache->pos = $pos;

			$iterator[$pos] = $userCache->_id;
			$success = true;
			PositionsModel::hMset($gameId, $iterator->getArrayCopy());
			ManagersModel::zIncrBy($manager->getManagerId(), 1, $gameId);
		}
		else{
			throw new \Exception('Position is not reserved: '.$pos);
		}

		return $success;
	}

	/**
	 * @param Manager        $manager
	 * @param string         $gameId
	 * @param                $pos
	 * @param UserCacheModel $userCache
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function reserved(Manager $manager, string $gameId, $pos, UserCacheModel $userCache):bool
	{
		$players = PositionsModel::hGetAll($gameId);

		$iterator = new PositionsIterator($players, $manager->getSeats());
		if ($pos === null) {
			$poss = $iterator->getArrayCopy();
			foreach ($poss as $pos => $u) {
				if (!$u) {
					break;
				}
			}
		}

		if (empty($iterator[$pos])) {
			$userCache->pos = $pos;

			$iterator[$pos] = PositionsIterator::RESERVED;
			$success = true;
			PositionsModel::hMset($gameId, $iterator->getArrayCopy());
			ManagersModel::zIncrBy($manager->getManagerId(), 1, $gameId);
		}
		else{
			throw new \Exception('Position is not free: '.$pos);
		}

		return $success;
	}

	/**
	 * @param string $gameId
	 * @param string $player
	 */
	public static function sitout(string $gameId, string $player)
	{
		SitoutedPlayers::hSet($gameId, $player, true);
	}

	/**
	 * @param string $gameId
	 * @param string $player
	 */
	public static function exitSitout(string $gameId, string $player)
	{
		SitoutedPlayers::hDel($gameId, $player);
	}

	/**
	 * @param Manager $manager
	 * @param string $gameId
	 * @param string $pos
	 * @param UserCacheModel $userCache
	 * @return bool
	 */
	public static function stand(Manager $manager, string $gameId, string $pos, UserCacheModel $userCache): bool
	{
		$success = false;
		$players = PositionsModel::hGetAll($gameId);

		$iterator = new PositionsIterator($players, $manager->getSeats());
		if (!empty($iterator[$pos]) && ($iterator[$pos] === $userCache->_id || $iterator[$pos] === PositionsIterator::RESERVED)) {
			$userCache->pos = '';

			unset($iterator[$pos]);
			$success = true;
			PositionsModel::hMset($gameId, $iterator->getArrayCopy());
			ManagersModel::zIncrBy($manager->getManagerId(), -1, $gameId);
		}

		return $success;
	}
}