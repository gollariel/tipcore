<?php
namespace TIP\Core\TableHelpers\Position;

use ArrayAccess;
use Countable;
use SeekableIterator;

/**
 * Class PositionsIterator
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PositionsIterator implements SeekableIterator, Countable, ArrayAccess
{
	public static $defaultPositions = [
		5 => [
			'pos1' => null,
			'pos2' => null,
			'pos3' => null,
			'pos4' => null,
			'pos5' => null,
		],
		9 => [
			'pos1' => null,
			'pos2' => null,
			'pos3' => null,
			'pos4' => null,
			'pos5' => null,
			'pos6' => null,
			'pos7' => null,
			'pos8' => null,
			'pos9' => null,
		],
	];

	public static $defaultRealNamePositions = [
		5 => [
			'sb',
			'bb',
			'co',
			'utg1',
			'btn',
		],
		9 => [
			'sb',
			'bb',
			'co',
			'utg1',
			'utg2',
			'utg3',
			'mp1',
			'mp2',
			'btn',
		],
	];

	protected $current = 0;
	protected $positions = [];
	protected $keys = [];
	protected $sits = 0;

	/**
	 * @param $position
	 * @param $seats
	 * @return bool
	 */
	public static function checkCorrectPosition($position, $seats) {
		return array_key_exists($position, static::$defaultPositions[$seats]??[]);
	}

	/**
	 * PositionsIterator constructor.
	 *
	 * @param array $array
	 * @param int $sits
	 * @throws \Exception
	 */
	public function __construct(Array $array = [], $sits = 5)
	{
		if (!isset(static::$defaultPositions[$sits])) {
			throw new \Exception('Not found sits: ' . $sits);
		}
		$this->sits = $sits;
		$this->reset($array);
	}

	/**
	 * @param $userId
	 *
	 * @return mixed|null
	 */
	public function getPositionByUser($userId) {
		$key = array_search($userId, $this->positions);
		if($key !== false) {
			return $key;
		}
		return null;
	}

	public static function getPositionName($seats, $sbPos, $heroPos)
	{
		$posR = static::$defaultPositions[$seats]??[];
		$posN = static::$defaultRealNamePositions[$seats]??[];

		if (count($posN) !== count($posR)) {
			return null;
		}
		$array = new \ArrayIterator($posR);

		if ($sbPos + 1 > $array->count()) {
			$sbPos = 0;
		}

		$array->seek($sbPos);
		$first = $array->key();
		$realPositions = [];

		while (true) {
			if ($array->valid()) {
				$realPositions[$array->key()] = array_shift($posN);
				$array->next();
			} else {
				$array->rewind();
			}

			if ($first === $array->key()) {
				break;
			}
		}

		return $realPositions[$heroPos]??null;
	}

	/**
	 * @param       $array
	 * @param array $without
	 * @throws \Exception
	 */
	public function init($array, $without = [])
	{
		foreach ($array as $offset => $value) {
			if (!isset($without[$value])) {
				$this->offsetSet($offset, $value);
			}
		}

		$this->keys = array_keys($this->getNotEmpty());
	}

	/**
	 * @param       $array
	 * @param array $without
	 * @throws \Exception
	 */
	public function reset($array, $without = [])
	{
		$sits = $this->sits;
		$this->positions = static::$defaultPositions[$sits];
		$this->keys = [];

		$this->init($array, $without);
	}

	const RESERVED = 'reserved';

	/**
	 * @return array
	 */
	public function getNotEmpty()
	{
		$players = [];
		foreach ($this->positions as $pos => $player) {
			if (!empty($player) && $player !== self::RESERVED) {
				$players[$pos] = $player;
			}
		}

		return $players;
	}

	/**
	 * @return array
	 */
	public function getWithoutReserve()
	{
		$players = [];
		foreach ($this->positions as $pos => $player) {
			if ($player === self::RESERVED) {
				$players[$pos] = null;
			} else {
				$players[$pos] = $player;
			}
		}

		return $players;
	}

	/**
	 * @param int $position
	 */
	public function seek($position)
	{
		$this->current += $position;
	}

	/**
	 * @param string $positionName
	 * @throws \Exception
	 */
	public function seekToPositionName($positionName)
	{
		if ($this->positionExists($positionName)) {
			$this->current = array_search($positionName, $this->keys);
		} else {
			throw new \Exception('Incorrect positionName');
		}
	}

	/**
	 * @return array
	 */
	public function getArrayCopy()
	{
		return $this->positions;
	}

	/**
	 *
	 */
	public function current()
	{
		$key = isset($this->keys[$this->current]) ? $this->keys[$this->current] : null;

		return isset($this->positions[$key]) ? $this->positions[$key] : null;
	}

	public function next()
	{
		$this->current++;
	}

	public function nextWithRewind()
	{
		$this->current++;
		if (!isset($this->keys[$this->current])) {
			$this->current = 0;
		}
	}

	/**
	 *
	 */
	public function key()
	{
		return isset($this->keys[$this->current]) ? $this->keys[$this->current] : null;
	}

	/**
	 * @return bool
	 */
	public function valid()
	{
		return isset($this->keys[$this->current]);
	}

	public function rewind()
	{
		$this->current = 0;
	}

	/**
	 * @return int
	 */
	public function count()
	{
		return count($this->keys);
	}

	/**
	 * @param $offset
	 * @return bool
	 */
	public function positionExists($offset)
	{
		return (array_key_exists($offset, $this->positions));
	}

	public function delete()
	{
		$this->offsetUnset($this->key());
		$this->current--;
	}

	public function previous()
	{
		$this->current--;
	}

	/**
	 * @param mixed $offset
	 * @return bool
	 * @throws \Exception
	 */
	public function offsetExists($offset)
	{
		if ($this->positionExists($offset)) {
			return !empty($this->positions[$offset]);
		} else {
			return false;
		}
	}

	/**
	 * @param mixed $offset
	 * @return mixed
	 * @throws \Exception
	 */
	public function offsetGet($offset)
	{
		if ($this->positionExists($offset)) {
			return $this->positions[$offset];
		} else {
			return null;
		}
	}

	/**
	 * @param mixed $offset
	 * @param mixed $value
	 * @throws \Exception
	 */
	public function offsetSet($offset, $value)
	{
		if ($this->positionExists($offset)) {
			$this->positions[$offset] = $value;
			$this->keys = array_keys($this->getNotEmpty());
		}
	}

	/**
	 * @param mixed $offset
	 * @throws \Exception
	 */
	public function offsetUnset($offset)
	{
		if ($this->positionExists($offset)) {
			$this->positions[$offset] = null;
			$this->keys = array_keys($this->getNotEmpty());
		}
	}

	/**
	 * @param $seek
	 */
	public function toDealer($seek)
	{
		$this->rewind();
		$this->seek($seek);

		if ($this->key() === null) {
			$this->nextWithRewind();
		}
	}

	/**
	 * @param $seek
	 */
	public function toSmallBlind($seek)
	{
		$this->toDealer($seek);
		if ($this->count() > 2) {
			$this->nextWithRewind();
		}
	}

	/**
	 * @param $seek
	 */
	public function toBigBlind($seek)
	{
		$this->toSmallBlind($seek);
		if ($this->count() > 2) {
			$this->nextWithRewind();
		}
	}
}