<?php
namespace TIP\Core\TableHelpers;

/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 2/8/17
 * Time: 22:20
 */
class Manager
{
	protected $managerId;
	protected $blind;
	protected $seats;
	protected $speed;
	protected $type;

	public static function generate($blind, $seats, $speed, $type){
		return $blind.':'.$seats.':'.$speed.':'.$type;
	}

	public function __construct($managerId)
	{
		$this->managerId = $managerId;
		$tmp = explode(':', $managerId);
		$managerList = ManagerHelper::getManagerList();
		if (count($tmp) === 4 && isset($managerList[$tmp[3]][$tmp[2]][$tmp[1]][$tmp[0]])) {
			list($blind, $seats, $speed, $type) = $tmp;
			$this->blind = $blind;
			$this->seats = $seats;
			$this->speed = $speed;
			$this->type = $type;
		} else {
			throw new \Exception('Not found manager');
		}
	}

	public function getManagerId()
	{
		return $this->managerId;
	}

	public function getBlind()
	{
		return $this->blind;
	}

	public function getSeats()
	{
		return $this->seats;
	}

	public function getSpeed()
	{
		return $this->speed;
	}

	public function getType()
	{
		return $this->type;
	}

	public function __toString()
	{
		return $this->managerId;
	}
}