<?php
namespace TIP\Core\TableHelpers;

use TkachInc\Core\Config\Config;

/**
 * Class ManagerHelper
 *
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ManagerHelper
{
	protected $managerList = [];
	protected static $instance;

	public function __construct()
	{
		$config = Config::getInstance()->loadDB('game');

		$this->managerList = $config->get(['managers_config'], []);
	}

	public static function __callStatic($name, $arguments)
	{
		if (!isset(static::$instance)) {
			static::$instance = new static();
		}
		return static::$instance->{$name}(...$arguments);
	}

	private function getManagerList()
	{
		return $this->managerList;
	}

	private function validateManagerId($managerId)
	{
		$tmp = explode(':', $managerId);

		if (count($tmp) === 4 && isset($this->managerList[$tmp[3]][$tmp[2]][$tmp[1]][$tmp[0]])) {
			list($blind, $seats, $speed, $type) = $tmp;
			return ['blind' => $blind, 'seats' => $seats, 'speed' => $speed, 'type' => $type];
		} else {
			return null;
		}
	}
}
