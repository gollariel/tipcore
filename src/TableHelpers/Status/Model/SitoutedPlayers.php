<?php
namespace TIP\Core\TableHelpers\Status\Model;
use TkachInc\Core\Database\Redis\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class SitoutedPlayers extends ObjectModel
{
	protected static $_separator = ':';
	protected static $_keyName = 'pk_sitouted_players';

	/**
	 * @param $id
	 * @return array
	 */
	public static function hGetAll($id)
	{
		$key = static::makePk($id);
		$fields = (array)static::getRedis()->hGetAll($key);

		return $fields;
	}

	/**
	 * @param $id
	 * @param $hashKeys
	 * @return bool
	 */
	public static function hMset($id, Array $hashKeys)
	{
		$key = static::makePk($id);

		return static::getRedis()->hMset($key, $hashKeys);
	}

	/**
	 * @param $id
	 * @param $hashKeys
	 * @return array
	 */
	public static function hMGet($id, Array $hashKeys)
	{
		$key = static::makePk($id);
		$fields = (array)static::getRedis()->hMGet($key, $hashKeys);

		return $fields;
	}
} 