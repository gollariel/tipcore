<?php
namespace TIP\Core\TableHelpers\Status\Model;

use TIP\Core\RedisAdapter\ObjectModelLists;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class WatchedPlayers extends ObjectModelLists
{
	protected static $_separator = ':';
	protected static $_keyName = 'pk_watched_players';
} 