<?php
namespace TIP\Core\TableHelpers;

use TkachInc\Engine\Services\Helpers\Base64URL;
use TkachInc\Engine\Services\Helpers\Generator\HashGenerator;

/**
 * Created by PhpStorm.
 * User: maxim.tkach
 * Date: 3/1/17
 * Time: 00:21
 */
class GameId
{
	protected $gameId;
	protected $managerId;

	public static function generate($managerId)
	{
		$prefix = Base64URL::encode($managerId);
		$length = dechex(strlen($prefix));
		return $length.'|'.$prefix . HashGenerator::getStringUniqueId();
	}

	public function __construct($gameId)
	{
		$this->gameId = $gameId;
		$tmp = explode('|', $gameId);
		if (count($tmp) !== 2) {
			throw new \Exception('Not found gameId');
		}
		list($length, $id) = $tmp;
		$length = hexdec($length);
		$managerId = substr($id, 0, $length);
		$this->managerId = Base64URL::decode($managerId);
	}

	public function getGameId()
	{
		return $this->gameId;
	}

	public function getManagerId()
	{
		return $this->managerId;
	}

	public function __toString()
	{
		return $this->gameId;
	}
}