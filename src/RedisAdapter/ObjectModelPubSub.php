<?php
namespace TIP\Core\RedisAdapter;
use TkachInc\Core\Database\Redis\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ObjectModelPubSub extends ObjectModel
{
	/**
	 * @param $channel
	 * @param $message
	 * @return int
	 */
	public static function publish($channel, $message)
	{
		return static::getRedis()->publish($channel, $message);
	}

	/**
	 * Callable function get: $redis, $chan, $msg
	 *
	 * @param array $channels
	 * @param callable $function
	 * @return mixed
	 */
	public static function subscribe(Array $channels, Callable $function)
	{
		static::getRedis()->subscribe($channels, $function);
	}

	/**
	 * @param          $patterns
	 * @param callable $function
	 */
	public static function psubscribe($patterns, Callable $function)
	{
		static::getRedis()->psubscribe($patterns, $function);
	}
} 