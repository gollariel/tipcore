<?php
namespace TIP\Core\RedisAdapter;
use TkachInc\Core\Database\Redis\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class ObjectModelStrings extends ObjectModel
{
	/**
	 * @param       $id
	 * @param       $value
	 * @param int $timeout
	 * @return bool
	 */
	public static function set($id, $value, $timeout = 0)
	{
		$key = static::makePk($id);

		return static::getRedis()->set($key, $value, $timeout);
	}

	/**
	 * @param $id
	 * @return bool|string
	 */
	public static function get($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->get($key);
	}

	/**
	 * @param $id
	 * @param $value
	 * @return string
	 */
	public static function getSet($id, $value)
	{
		$key = static::makePk($id);

		return static::getRedis()->getSet($key, $value);
	}

	/**
	 * @param $id
	 * @return bool|string
	 */
	public static function decr($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->decr($key);
	}

	/**
	 * @param $id
	 * @return bool|string
	 */
	public static function incr($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->incr($key);
	}
} 