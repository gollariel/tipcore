<?php
namespace TIP\Core\RedisAdapter;
use TkachInc\Core\Database\Redis\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class ObjectModelSets extends ObjectModel
{
	/**
	 * @param $id
	 * @param $value
	 * @return int
	 */
	public static function sAdd($id, $value)
	{
		$key = static::makePk($id);

		return static::getRedis()->sAdd($key, $value);
	}

	/**
	 * @param $id
	 * @return array
	 */
	public static function sMembers($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->sMembers($key);
	}

	/**
	 * @param $id
	 * @return string
	 */
	public static function sPop($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->sPop($key);
	}

	/**
	 * @param $id
	 * @param $value
	 * @return int
	 */
	public static function sRem($id, $value)
	{
		$key = static::makePk($id);

		return static::getRedis()->sRem($key, $value);
	}

	/**
	 * @param $id
	 * @return string
	 */
	public static function sRandmember($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->sRandMember($key);
	}

	/**
	 * @param $id
	 * @return int
	 */
	public static function sCard($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->sCard($key);
	}
} 