<?php
namespace TIP\Core\RedisAdapter;
use TkachInc\Core\Database\Redis\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class ObjectModelSortedSets extends ObjectModel
{
	/**
	 * @param $id
	 * @param $item
	 * @param $value
	 * @return mixed
	 */
	public static function zAdd($id, $item, $value)
	{
		$key = static::makePk($id);

		return static::getRedis()->zAdd($key, $value, $item);
	}

	/**
	 * @param      $id
	 * @param      $start
	 * @param      $end
	 * @param null $withscores
	 * @return mixed
	 */
	public static function zRange($id, $start, $end, $withscores = null)
	{
		$key = static::makePk($id);

		return static::getRedis()->zRange($key, $start, $end, $withscores);
	}

	public static function zRank($id, $member)
	{
		$key = static::makePk($id);

		return static::getRedis()->zRank($key, $member);
	}


	/**
	 * @param       $id
	 * @param       $start
	 * @param       $end
	 * @param bool $withscore
	 * @return mixed
	 */
	public static function zRevRange($id, $start, $end, $withscore = null)
	{
		$key = static::makePk($id);

		return static::getRedis()->zRevRange($key, $start, $end, $withscore);
	}

	/**
	 * @param       $id
	 * @param       $start
	 * @param       $end
	 * @param array $options
	 * @return array
	 */
	public static function zRangeByScore($id, $start, $end, array $options = [])
	{
		$key = static::makePk($id);

		return static::getRedis()->zRangeByScore($key, $start, $end, $options);
	}

	/**
	 * @param       $id
	 * @param       $start
	 * @param       $end
	 * @param array $options
	 * @return array
	 */
	public static function zRevRangeByScore($id, $start, $end, array $options = [])
	{
		$key = static::makePk($id);

		return static::getRedis()->zRevRangeByScore($key, $start, $end, $options);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	public static function zCard($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->zCard($key);
	}

	/**
	 * @param $id
	 * @param $member
	 * @return mixed
	 */
	public static function zScore($id, $member)
	{
		$key = static::makePk($id);

		return static::getRedis()->zScore($key, $member);
	}

	/**
	 * @param $id
	 * @param $value
	 * @param $member
	 * @return mixed
	 */
	public static function zIncrBy($id, $value, $member)
	{
		$key = static::makePk($id);

		return static::getRedis()->zIncrBy($key, $value, $member);
	}

	/**
	 * @param $id
	 * @param $member
	 * @return int
	 */
	public static function zRem($id, $member)
	{
		$key = static::makePk($id);

		return static::getRedis()->zRem($key, $member);
	}
} 