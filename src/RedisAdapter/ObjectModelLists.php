<?php
namespace TIP\Core\RedisAdapter;
use TkachInc\Core\Database\Redis\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class ObjectModelLists extends ObjectModel
{
	/**
	 * @param $id
	 * @param $value
	 * @return int
	 */
	public static function rPush($id, $value)
	{
		$key = static::makePk($id);

		return static::getRedis()->rPush($key, $value);
	}

	/**
	 * @param $id
	 * @return string
	 */
	public static function rPop($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->rPop($key);
	}

	/**
	 * @param $id
	 * @param $value
	 * @return int
	 */
	public static function lPush($id, $value)
	{
		$key = static::makePk($id);

		return static::getRedis()->lPush($key, $value);
	}

	/**
	 * @param $id
	 * @return string
	 */
	public static function lPop($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->lPop($key);
	}

	/**
	 * @param $id
	 * @param $start
	 * @param $end
	 * @return array
	 */
	public static function lRange($id, $start, $end)
	{
		$key = static::makePk($id);

		return static::getRedis()->lRange($key, $start, $end);
	}

	/**
	 * @param $id
	 * @return int
	 */
	public static function lLen($id)
	{
		$key = static::makePk($id);

		return static::getRedis()->lLen($key);
	}

	/**
	 * @param     $id
	 * @param     $value
	 * @param int $count
	 * @return int
	 */
	public static function lRem($id, $value, $count = 0)
	{
		$key = static::makePk($id);

		return static::getRedis()->lRem($key, $value, $count);
	}

} 